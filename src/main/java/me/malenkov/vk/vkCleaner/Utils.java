package me.malenkov.vk.vkCleaner;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static me.malenkov.vk.vkCleaner.VkCleaner.MAX_SPLIT_COUNT;


class Utils {
    private static final Logger log = LogManager.getLogger(Utils.class);

    private static int id;
    private static String key;

    Utils() throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/key.properties");
        Properties props = new Properties();
        props.load(fis);

        String sid = props.getProperty("vk.id");
        String skey = props.getProperty("vk.key");

        if (StringUtils.isEmpty(sid)) {
            throw new RuntimeException("vk.id system property empty");
        }
        if (StringUtils.isEmpty(skey)) {
            throw new RuntimeException("vk.key system property empty");
        }

        id = Integer.parseInt(sid);
        key = skey;
        log.info("INIT: key loaded");
    }

    void pause(int pause) {
        log.info("will sleep " + pause + " sec");
        try {
            Thread.sleep(pause * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.interrupted();
        }
    }

    VkApiClient initClient() {
        TransportClient transportClient = HttpTransportClient.getInstance();
        return new VkApiClient(transportClient);
    }

    UserActor getActor() {
        return new UserActor(id, key);
    }

    List<String> listOfInt2listOfSting(List<Integer> data) {
        List<String> res = new ArrayList<>();
        for (Integer i : data) {
            res.add(String.valueOf(i));
        }
        return res;
    }

    Map<Integer, List<String>> splitList(List<String> data){
        Map<Integer, List<String>> res = new HashMap<>();

        int key = 0;
        int cnt = 0;

        List<String> ls = new ArrayList<>();
        for (String s : data) {
            ls.add(s);
            cnt++;

            if (cnt == MAX_SPLIT_COUNT) {
                cnt = 0;
                res.put(key, new ArrayList<>(ls));
                key++;
                ls.clear();
            }
        }
        if (ls.size() > 0) {
            key++;
            res.put(key, ls);
        }
        return res;
    }
}

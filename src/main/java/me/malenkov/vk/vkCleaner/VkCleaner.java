package me.malenkov.vk.vkCleaner;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class VkCleaner {
    private static final boolean CHECK_ONLY = false;
    static final int MAX_SPLIT_COUNT = 800;

    private VkApiClient vk;
    private UserActor actor;
    private Utils utils;

    private static final Logger log = LogManager.getLogger(VkCleaner.class);

    VkCleaner() throws ClientException, ApiException, IOException {
        utils = new Utils();
        vk = utils.initClient();
        actor = utils.getActor();

        log.info("=== will ban outgoing requests ===");

        List<Integer> oreqsIds = vk.friends().getRequests(actor).out(true).execute().getItems();
        List<String> oreqsStr = utils.listOfInt2listOfSting(oreqsIds);
        log.info(oreqsStr.size() + " requests loaded");

        if (oreqsStr.size() > 0) {
            List<UserXtrCounters> oreqs = loadUsers(oreqsStr);

            log.info("loaded " + oreqs.size() + " users to ban");

            for (UserXtrCounters u : oreqs) {
                if (CHECK_ONLY) {
                    log.info("can ban https://vk.com/" + u.getDomain() + " - " + u.getFirstName() + " " + u.getLastName());
                } else {
                    log.info("will ban https://vk.com/" + u.getDomain() + " - " + u.getFirstName() + " " + u.getLastName());
                    int r = vk.account().banUser(actor, u.getId()).execute().getValue();
                    log.info(u.getFirstName() + " " + u.getLastName() + " ban res: " + r);
                    utils.pause(3);
                }
            }
        } else {
            log.info("nothing to ban :)");
        }


        log.info("=== will remove deleted and banned friends ===");
        List<UserXtrCounters> friends = loadFriends();

        Multimap<String, UserXtrCounters> badUsers = ArrayListMultimap.create();
        for (UserXtrCounters f : friends) {
            if (f.getDeactivated() != null) {
                log.info("https://vk.com/" + f.getDomain() + " - " + f.getFirstName() + " " + f.getLastName() + " - " + f.getDeactivated());
                badUsers.put(f.getDeactivated(), f);
            }
        }

        log.info("will be deleted " + badUsers.size() + " locked friends");

        for (String k : badUsers.keySet()) {
            log.info(k + " " + badUsers.get(k).size());
        }

        for (String k : badUsers.keySet()) {
            for (UserXtrCounters u : badUsers.get(k)) {
                if (CHECK_ONLY) {
                    log.info("can remove https://vk.com/" + u.getDomain() + " - " + u.getFirstName() + " " + u.getLastName());
                } else {
                    log.info("will remove https://vk.com/" + u.getDomain() + " - " + u.getFirstName() + " " + u.getLastName());
                    int r = vk.friends().delete(actor, u.getId()).execute().getSuccess().getValue();
                    log.info(u.getFirstName() + " " + u.getLastName() + " delete res: " + r);
                    utils.pause(3);
                }
            }
        }

    }

    private List<UserXtrCounters> loadFriends() throws ClientException, ApiException {
        List<Integer> frIds = vk.friends().get(actor).count(10000).execute().getItems();
        List<String> frIdsStr = utils.listOfInt2listOfSting(frIds);
        log.info("friends count = " + frIds.size());

        List<UserXtrCounters> friends = loadUsers(frIdsStr);

        log.info("finally loaded " + friends.size() + " friends");
        return friends;
    }

    private List<UserXtrCounters> loadUsers(List<String> ids) throws ClientException, ApiException {
        List<UserXtrCounters> users = new ArrayList<>();

        Map<Integer, List<String>> fmap = utils.splitList(ids);

        for (int k : fmap.keySet()) {
            log.info(k + ": " + fmap.get(k).size());

            log.info("map to load size = " + fmap.get(k).size());
            List<UserXtrCounters> frds = vk.users().get(actor).fields(UserField.DOMAIN).userIds(fmap.get(k)).execute();
            log.info("loaded " + frds.size() + " users");

            users.addAll(frds);
            utils.pause(3);
        }

        return users;
    }
}
